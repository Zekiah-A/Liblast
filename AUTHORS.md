# Liblast, a libre multiplayer FPS game
Copyleft (ɔ) 2020-2021 Liblast Team, Liblast contributors

## Liblast Team members

- Tobiasz 'unfa' Karoń
- Pablo 'Pablinski2' Pozo
- JM 'aRdem'
- Jan 'Combustible Lemonade' Heemstra
- Adham 'gilgamesh' Omran
- John 'nebunez' Ryan

### Past Team members

(none)

## Liblast contributors

(none)

### Past contributors

(none)
