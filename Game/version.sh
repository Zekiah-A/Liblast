#!/bin/bash
echo -n "$(git log -n1 | head -n3 | tail -n1) · $(git status | grep "On branch" | cut -d' ' -f 2-) · commit hash: $(git reflog -n1 | cut -d' ' -f1)" > version 2>&1
