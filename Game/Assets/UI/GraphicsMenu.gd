extends "res://Assets/UI/Menu.gd"

func on_fullscreen_toggled(button_pressed):
	if GUI:
		GUI.set_fullscreen(button_pressed)
		$Fullscreen.save_data()

func on_lighting_toggled(button_pressed):
	if GUI:
		GUI.set_lighting(button_pressed)
		$Lighting.save_data()

func on_ambient_occlusion_toggled(button_pressed):
	if GUI:
		GUI.set_ambient_occlusion(button_pressed)
		$AmbientOcclusion.save_data()

func on_shadows_toggled(button_pressed):
	if GUI:
		GUI.set_shadows(button_pressed)
		$Shadows.save_data()

func on_global_illumination_toggled(button_pressed):
	if GUI:
		GUI.set_global_illumination(button_pressed)
		$GlobalIllumination.save_data()

func on_screen_space_reflections_toggled(button_pressed):
	if GUI:
		GUI.set_screen_space_reflections(button_pressed)
		$ScreenSpaceReflections.save_data()

func on_reflection_probes_toggled(button_pressed):
	if GUI:
		GUI.set_reflection_probes(button_pressed)
		$ReflectionProbes.save_data()

func on_glow_toggled(button_pressed):
	if GUI:
		GUI.set_glow(button_pressed)
		$Glow.save_data()

func on_MSAA_toggled(button_pressed):
	if GUI:
		GUI.set_MSAA(button_pressed)
		$MSAA.save_data()

func on_FXAA_toggled(button_pressed):
	if GUI:
		GUI.set_FXAA(button_pressed)
		$FXAA.save_data()

func on_debanding_toggled(button_pressed):
	if GUI:
		GUI.set_debanding(button_pressed)
		$Debanding.save_data()

func on_decals_static_toggled(button_pressed):
	if GUI:
		GUI.set_decals_static(button_pressed)
		$DecalsStatic.save_data()

func on_decals_decals_toggled(button_pressed):
	if GUI:
		GUI.set_decals_decals(button_pressed)
		$DecalsDecals.save_data()

func on_dynamic_lights_toggled(button_pressed):
	if GUI:
		GUI.set_dynamic_lights(button_pressed)
		$DynamicLights.save_data()

func on_FSR_toggled(button_pressed):
	if GUI:
		GUI.set_fsr(button_pressed)
		$FSR.save_data()

func _on_RenderScale_value_changed(value):
	var value_map = {
			0: 0.25,
			1: 0.5,
			2: 0.75,
			3: 1.0,
			4: 2.0,
			5: 4.0,
			6: 8.0,
			}
	if GUI:
		GUI.set_render_scale(value_map.get(value as int))
		$RenderScale.save_data()
