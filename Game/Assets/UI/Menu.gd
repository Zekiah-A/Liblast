extends VBoxContainer

var previous_menu : Node = null
var settings = {}
@onready var GUI = get_parent().get_parent()
@onready var Main = get_parent().get_parent().get_parent()

# Called when the node enters the scene tree for the first time.
func _ready():
	if previous_menu == null:
		$Back.hide()

func open_menu(path : String):
	var menu = load(path).instantiate()
	menu.previous_menu = self
	get_parent().add_child(menu)
	hide()

func go_back():
	$Back/ClickSound.play()
	previous_menu.show()
	previous_menu = null
	queue_free()

func _on_Back_mouse_entered():
	$Back/HoverSound.play()
