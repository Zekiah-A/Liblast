extends "res://Assets/UI/MenuData.gd"

func set_data(_data):
	super.set_data(_data)
	if not $LineEdit.has_focus():
		$LineEdit.text = _data

func on_label_changed():
	$Label.text = label
