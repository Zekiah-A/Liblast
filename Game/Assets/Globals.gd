extends Node

enum DamageType {
	NONE,
	BULLET,
	EXPLOSION,
	ENVIRONMENT_HAZARD,
}

#enum MaterialType { NONE,
#					CONCRETE,
#					METAL,
#					WOOD,
#					GLASS,
#					WATER,
#					}
