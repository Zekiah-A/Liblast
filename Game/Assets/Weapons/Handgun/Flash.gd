extends Node3D

func _ready():
	#print("Muzzle flash spawned")
	$GPUParticles3D.emitting = true
	$AnimationPlayer.play("Flash")
	

func _on_Timer_timeout():
	queue_free()
