extends Control

@onready var main = get_tree().get_root().get_node("Main")

var pain: float = 0:
	set(value):
		$PostProcessing/Damage.material.set('shader_param/Damage', value)
	get:
		return $PostProcessing/Damage.material.get('shader_param/Damage')

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func game_over(winner):
	scoretab(true, winner)

func damage(hp):
	print("HUD damage ", hp)
	pain += hp/20

func hide():
	$Crosshair.hide()
	$Chat.hide()

func show():
	$Crosshair.show()
	$Chat.show()
	
func get_pid_info(pid: int) -> Dictionary:
		var player_name = main.player_list.get(pid).name
		var player_color = main.player_list.get(pid).color
		var ping = main.player_list.get(pid).ping
		if not ping:
			ping = "?"
		var packet_loss = main.player_list.get(pid).packet_loss
		if not packet_loss:
			packet_loss = "?"
		return {
				'name' = player_name,
				'color' = player_color,
				'ping' = ping,
				'loss' = packet_loss,
				}
		
func update_scoretab():
	$ScoreTable/VBoxContainer/ScoreTab.text = ''
	
	var scores = []
		
	for pid in main.player_list.players.keys():
		scores.append(main.player_list.get(pid).score)
	
	scores.sort()
	scores.reverse()
	
	var done = []
	
	for score in scores:
		for pid in main.player_list.players.keys():
			if main.player_list.get(pid).score == score:
				if pid not in done:
					var info = get_pid_info(pid)
#					var j_name =  main.player_list.get(j).name
#					var j_peer = get_tree().multiplayer.multiplayer_peer.get_peer(j as int)
#					var j_ping = "?"
#					var j_packet_loss = "?"
#					if j_peer:
#						j_ping = j_peer.get_statistic(ENetPacketPeer.PEER_ROUND_TRIP_TIME)
#						j_packet_loss = j_peer.get_statistic(ENetPacketPeer.PEER_PACKET_LOSS)
					$ScoreTable/VBoxContainer/ScoreTab.text += "[b]" + str(score) + "[color=" + Color(info['color']).to_html() + "] - " + info['name'] + "[/color][/b]" + " (" + str(info['ping']) + " ms · " + str(info['loss']) +")\n"
					done.append(pid)

func scoretab(show: bool, winner = null):
	if show:
		update_scoretab()
		$ScoreTable.show()
	else:
		$ScoreTable.hide()
	
	if winner:
		$ScoreTable/VBoxContainer/Header.text = "Match over! Player " + main.player_list.get(winner).name + " won!"
	else:
		$ScoreTable/VBoxContainer/Header.text = "Playing deathmatch until " + str(main.game_score_limit) + " kills"
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	if main.local_player == null: # player is dead ATM
#	print("local_player is null; PID is ", get_tree().multiplayer.get_multiplayer_unique_id(),"; respawn queue: ", main.respawn_queue)
	if not get_tree().multiplayer.has_multiplayer_peer(): # don't do anything if we're offline
		return
	
	if main.spawn_queue.has(get_tree().multiplayer.get_unique_id()):
		var countdown = main.spawn_queue[get_tree().multiplayer.get_unique_id()] - main.uptime
#		countdown = round(countdown * 10) / 10
		$RespawnCountdown.text = "RESPAWNING IN " + str("%1.2f" % countdown) + " SECONDS..."
		$RespawnCountdown.visible = true
	else:
		$RespawnCountdown.visible = false
	
	if $ScoreTable.visible: # update the scores every frame when player is watching the score table
		update_scoretab()
	
	if main.local_player:
		if not main.local_player.dead:
			pain *= 1 - delta # time heals pain, as long as you're alive
