shader_type spatial;

render_mode unshaded, blend_add;

uniform vec3 uv1_scale = vec3(1.0, 1.0, 1.0);
uniform vec3 uv1_offset = vec3(0.0, 0.0, 0.0);
varying float elapsed_time;
void vertex() {
	elapsed_time = TIME;
	UV = UV*uv1_scale.xy+uv1_offset.xy;
}
float rand(vec2 x) {
    return fract(cos(mod(dot(x, vec2(13.9898, 8.141)), 3.14)) * 43758.5453);
}

vec2 rand2(vec2 x) {
    return fract(cos(mod(vec2(dot(x, vec2(13.9898, 8.141)),
						      dot(x, vec2(3.4562, 17.398))), vec2(3.14))) * 43758.5453);
}

vec3 rand3(vec2 x) {
    return fract(cos(mod(vec3(dot(x, vec2(13.9898, 8.141)),
							  dot(x, vec2(3.4562, 17.398)),
                              dot(x, vec2(13.254, 5.867))), vec3(3.14))) * 43758.5453);
}

vec3 rgb2hsv(vec3 c) {
	vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
	vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}


uniform sampler2D texture_1;
const float texture_1_size = 512.0;

uniform sampler2D texture_2;
const float texture_2_size = 512.0;

float shape_circle(vec2 uv, float sides, float size, float edge) {
    uv = 2.0*uv-1.0;
	edge = max(edge, 1.0e-8);
    float distance = length(uv);
    return clamp((1.0-distance/size)/edge, 0.0, 1.0);
}

float shape_polygon(vec2 uv, float sides, float size, float edge) {
    uv = 2.0*uv-1.0;
	edge = max(edge, 1.0e-8);
    float angle = atan(uv.x, uv.y)+3.14159265359;
    float slice = 6.28318530718/sides;
    return clamp((1.0-(cos(floor(0.5+angle/slice)*slice-angle)*length(uv))/size)/edge, 0.0, 1.0);
}

float shape_star(vec2 uv, float sides, float size, float edge) {
    uv = 2.0*uv-1.0;
	edge = max(edge, 1.0e-8);
    float angle = atan(uv.x, uv.y);
    float slice = 6.28318530718/sides;
    return clamp((1.0-(cos(floor(angle*sides/6.28318530718-0.5+2.0*step(fract(angle*sides/6.28318530718), 0.5))*slice-angle)*length(uv))/size)/edge, 0.0, 1.0);
}

float shape_curved_star(vec2 uv, float sides, float size, float edge) {
    uv = 2.0*uv-1.0;
	edge = max(edge, 1.0e-8);
    float angle = 2.0*(atan(uv.x, uv.y)+3.14159265359);
    float slice = 6.28318530718/sides;
    return clamp((1.0-cos(floor(0.5+0.5*angle/slice)*2.0*slice-angle)*length(uv)/size)/edge, 0.0, 1.0);
}

float shape_rays(vec2 uv, float sides, float size, float edge) {
    uv = 2.0*uv-1.0;
	edge = 0.5*max(edge, 1.0e-8)*size;
	float slice = 6.28318530718/sides;
    float angle = mod(atan(uv.x, uv.y)+3.14159265359, slice)/slice;
    return clamp(min((size-angle)/edge, angle/edge), 0.0, 1.0);
}


vec3 blend_normal(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*c1 + (1.0-opacity)*c2;
}

vec3 blend_dissolve(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	if (rand(uv) < opacity) {
		return c1;
	} else {
		return c2;
	}
}

vec3 blend_multiply(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*c1*c2 + (1.0-opacity)*c2;
}

vec3 blend_screen(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*(1.0-(1.0-c1)*(1.0-c2)) + (1.0-opacity)*c2;
}

float blend_overlay_f(float c1, float c2) {
	return (c1 < 0.5) ? (2.0*c1*c2) : (1.0-2.0*(1.0-c1)*(1.0-c2));
}

vec3 blend_overlay(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*vec3(blend_overlay_f(c1.x, c2.x), blend_overlay_f(c1.y, c2.y), blend_overlay_f(c1.z, c2.z)) + (1.0-opacity)*c2;
}

vec3 blend_hard_light(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*0.5*(c1*c2+blend_overlay(uv, c1, c2, 1.0)) + (1.0-opacity)*c2;
}

float blend_soft_light_f(float c1, float c2) {
	return (c2 < 0.5) ? (2.0*c1*c2+c1*c1*(1.0-2.0*c2)) : 2.0*c1*(1.0-c2)+sqrt(c1)*(2.0*c2-1.0);
}

vec3 blend_soft_light(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*vec3(blend_soft_light_f(c1.x, c2.x), blend_soft_light_f(c1.y, c2.y), blend_soft_light_f(c1.z, c2.z)) + (1.0-opacity)*c2;
}

float blend_burn_f(float c1, float c2) {
	return (c1==0.0)?c1:max((1.0-((1.0-c2)/c1)),0.0);
}

vec3 blend_burn(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*vec3(blend_burn_f(c1.x, c2.x), blend_burn_f(c1.y, c2.y), blend_burn_f(c1.z, c2.z)) + (1.0-opacity)*c2;
}

float blend_dodge_f(float c1, float c2) {
	return (c1==1.0)?c1:min(c2/(1.0-c1),1.0);
}

vec3 blend_dodge(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*vec3(blend_dodge_f(c1.x, c2.x), blend_dodge_f(c1.y, c2.y), blend_dodge_f(c1.z, c2.z)) + (1.0-opacity)*c2;
}

vec3 blend_lighten(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*max(c1, c2) + (1.0-opacity)*c2;
}

vec3 blend_darken(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*min(c1, c2) + (1.0-opacity)*c2;
}

vec3 blend_difference(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*clamp(c2-c1, vec3(0.0), vec3(1.0)) + (1.0-opacity)*c2;
}

vec4 adjust_levels(vec4 input, vec4 in_min, vec4 in_mid, vec4 in_max, vec4 out_min, vec4 out_max) {
	input = clamp((input-in_min)/(in_max-in_min), 0.0, 1.0);
	in_mid = (in_mid-in_min)/(in_max-in_min);
	vec4 dark = step(in_mid, input);
	input = 0.5*mix(input/(in_mid), 1.0+(input-in_mid)/(1.0-in_mid), dark);
	return out_min+input*(out_max-out_min);
}

vec3 rgb_to_hsv(vec3 c) {
	vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
	vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv_to_rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec2 scale(vec2 uv, vec2 center, vec2 scale) {
	uv -= center;
	uv /= scale;
	uv += center;
    return uv;
}
const float p_o425155_cx = 0.000000000;
const float p_o425155_cy = 0.000000000;
const float p_o425155_scale_x = 1.825000000;
const float p_o425155_scale_y = 1.825000000;
const float p_o9600_hue = 0.000000000;
const float p_o9600_saturation = 1.919300000;
const float p_o9600_value = 1.000000000;
vec4 o9600_f(vec4 c) {
	vec3 hsv = rgb_to_hsv(c.rgb);
	return vec4(hsv_to_rgb(vec3(fract(hsv.x+p_o9600_hue), clamp(hsv.y*p_o9600_saturation, 0.0, 1.0), clamp(hsv.z*p_o9600_value, 0.0, 1.0))), c.a);
}const float p_o9579_gradient_0_pos = 0.000000000;
const float p_o9579_gradient_0_r = 0.000000000;
const float p_o9579_gradient_0_g = 0.000000000;
const float p_o9579_gradient_0_b = 0.000000000;
const float p_o9579_gradient_0_a = 1.000000000;
const float p_o9579_gradient_1_pos = 0.377056000;
const float p_o9579_gradient_1_r = 0.000000000;
const float p_o9579_gradient_1_g = 0.718750000;
const float p_o9579_gradient_1_b = 1.000000000;
const float p_o9579_gradient_1_a = 1.000000000;
const float p_o9579_gradient_2_pos = 0.777056000;
const float p_o9579_gradient_2_r = 0.551358998;
const float p_o9579_gradient_2_g = 0.878682971;
const float p_o9579_gradient_2_b = 1.000000000;
const float p_o9579_gradient_2_a = 1.000000000;
const float p_o9579_gradient_3_pos = 1.000000000;
const float p_o9579_gradient_3_r = 1.000000000;
const float p_o9579_gradient_3_g = 1.000000000;
const float p_o9579_gradient_3_b = 1.000000000;
const float p_o9579_gradient_3_a = 1.000000000;
vec4 o9579_gradient_gradient_fct(float x) {
  if (x < p_o9579_gradient_0_pos) {
    return vec4(p_o9579_gradient_0_r,p_o9579_gradient_0_g,p_o9579_gradient_0_b,p_o9579_gradient_0_a);
  } else if (x < p_o9579_gradient_1_pos) {
    return mix(vec4(p_o9579_gradient_0_r,p_o9579_gradient_0_g,p_o9579_gradient_0_b,p_o9579_gradient_0_a), vec4(p_o9579_gradient_1_r,p_o9579_gradient_1_g,p_o9579_gradient_1_b,p_o9579_gradient_1_a), ((x-p_o9579_gradient_0_pos)/(p_o9579_gradient_1_pos-p_o9579_gradient_0_pos)));
  } else if (x < p_o9579_gradient_2_pos) {
    return mix(vec4(p_o9579_gradient_1_r,p_o9579_gradient_1_g,p_o9579_gradient_1_b,p_o9579_gradient_1_a), vec4(p_o9579_gradient_2_r,p_o9579_gradient_2_g,p_o9579_gradient_2_b,p_o9579_gradient_2_a), ((x-p_o9579_gradient_1_pos)/(p_o9579_gradient_2_pos-p_o9579_gradient_1_pos)));
  } else if (x < p_o9579_gradient_3_pos) {
    return mix(vec4(p_o9579_gradient_2_r,p_o9579_gradient_2_g,p_o9579_gradient_2_b,p_o9579_gradient_2_a), vec4(p_o9579_gradient_3_r,p_o9579_gradient_3_g,p_o9579_gradient_3_b,p_o9579_gradient_3_a), ((x-p_o9579_gradient_2_pos)/(p_o9579_gradient_3_pos-p_o9579_gradient_2_pos)));
  }
  return vec4(p_o9579_gradient_3_r,p_o9579_gradient_3_g,p_o9579_gradient_3_b,p_o9579_gradient_3_a);
}
const float p_o111395_amount = 1.000000000;
const float p_o305707_amount = 0.045000000;
const float p_o305707_eps = 0.120000000;
const float p_o9577_translate_x = 0.000000000;
float o305707_input_d(vec2 uv) {
vec4 o9596_0 = textureLod(texture_1, (uv)-vec2(p_o9577_translate_x, (- elapsed_time / 3.0)), 0.0);
vec4 o9577_0_1_rgba = o9596_0;

return (dot((o9577_0_1_rgba).rgb, vec3(1.0))/3.0);
}
vec2 o305707_slope(vec2 uv, float epsilon) {
	return vec2(o305707_input_d(fract(uv+vec2(epsilon, 0.0)))-o305707_input_d(fract(uv-vec2(epsilon, 0.0))), o305707_input_d(fract(uv+vec2(0.0, epsilon)))-o305707_input_d(fract(uv-vec2(0.0, epsilon))));
}const float p_o277932_amount = 0.010000000;
const float p_o277932_eps = 0.120000000;
const float p_o9578_translate_x = 0.000000000;
float o277932_input_d(vec2 uv) {
vec4 o9598_0 = textureLod(texture_2, (uv)-vec2(p_o9578_translate_x, (- elapsed_time / 6.0)), 0.0);
vec4 o9578_0_1_rgba = o9598_0;

return (dot((o9578_0_1_rgba).rgb, vec3(1.0))/3.0);
}
vec2 o277932_slope(vec2 uv, float epsilon) {
	return vec2(o277932_input_d(fract(uv+vec2(epsilon, 0.0)))-o277932_input_d(fract(uv-vec2(epsilon, 0.0))), o277932_input_d(fract(uv+vec2(0.0, epsilon)))-o277932_input_d(fract(uv-vec2(0.0, epsilon))));
}const float p_o154404_sides = 2.000000000;
const float p_o154404_radius = 1.000000000;
const float p_o154404_edge = 1.000000000;
const float p_o262174_default_in1 = 0.000000000;
const float p_o262174_default_in2 = 2.000000000;
const float p_o119625_curve_0_x = 0.000000000;
const float p_o119625_curve_0_y = 0.000000000;
const float p_o119625_curve_0_ls = 0.000000000;
const float p_o119625_curve_0_rs = 99.205083325;
const float p_o119625_curve_1_x = 0.020833334;
const float p_o119625_curve_1_y = 0.998410165;
const float p_o119625_curve_1_ls = 0.000000000;
const float p_o119625_curve_1_rs = -9.920508671;
const float p_o119625_curve_2_x = 0.199519426;
const float p_o119625_curve_2_y = 0.000000000;
const float p_o119625_curve_2_ls = 0.000000000;
const float p_o119625_curve_2_rs = -0.000000000;
const float p_o119625_curve_3_x = 1.000000000;
const float p_o119625_curve_3_y = 0.000000000;
const float p_o119625_curve_3_ls = 0.000000000;
const float p_o119625_curve_3_rs = -0.000000000;
float o119625_curve_curve_fct(float x) {
if (x <= p_o119625_curve_1_x) {
float dx = x - p_o119625_curve_0_x;
float d = p_o119625_curve_1_x - p_o119625_curve_0_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o119625_curve_0_y;
float yac = p_o119625_curve_0_y + d*p_o119625_curve_0_rs;
float ybc = p_o119625_curve_1_y - d*p_o119625_curve_1_ls;
float y2 = p_o119625_curve_1_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o119625_curve_2_x) {
float dx = x - p_o119625_curve_1_x;
float d = p_o119625_curve_2_x - p_o119625_curve_1_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o119625_curve_1_y;
float yac = p_o119625_curve_1_y + d*p_o119625_curve_1_rs;
float ybc = p_o119625_curve_2_y - d*p_o119625_curve_2_ls;
float y2 = p_o119625_curve_2_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o119625_curve_3_x) {
float dx = x - p_o119625_curve_2_x;
float d = p_o119625_curve_3_x - p_o119625_curve_2_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o119625_curve_2_y;
float yac = p_o119625_curve_2_y + d*p_o119625_curve_2_rs;
float ybc = p_o119625_curve_3_y - d*p_o119625_curve_3_ls;
float y2 = p_o119625_curve_3_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
}
const float p_o9549_color = 0.500000000;
const float p_o9625_amount = 0.420000000;
const float p_o9625_eps = 0.005000000;
const float p_o57402_amount = 0.030000000;
const float p_o54620_repeat = 64.000000000;
const float p_o54620_gradient_0_pos = 0.000000000;
const float p_o54620_gradient_0_r = 0.000000000;
const float p_o54620_gradient_0_g = 0.000000000;
const float p_o54620_gradient_0_b = 0.000000000;
const float p_o54620_gradient_0_a = 1.000000000;
const float p_o54620_gradient_1_pos = 0.454544345;
const float p_o54620_gradient_1_r = 1.000000000;
const float p_o54620_gradient_1_g = 1.000000000;
const float p_o54620_gradient_1_b = 1.000000000;
const float p_o54620_gradient_1_a = 1.000000000;
const float p_o54620_gradient_2_pos = 1.000000000;
const float p_o54620_gradient_2_r = 0.000000000;
const float p_o54620_gradient_2_g = 0.000000000;
const float p_o54620_gradient_2_b = 0.000000000;
const float p_o54620_gradient_2_a = 1.000000000;
vec4 o54620_gradient_gradient_fct(float x) {
  if (x < p_o54620_gradient_0_pos) {
    return vec4(p_o54620_gradient_0_r,p_o54620_gradient_0_g,p_o54620_gradient_0_b,p_o54620_gradient_0_a);
  } else if (x < p_o54620_gradient_1_pos) {
    return mix(vec4(p_o54620_gradient_0_r,p_o54620_gradient_0_g,p_o54620_gradient_0_b,p_o54620_gradient_0_a), vec4(p_o54620_gradient_1_r,p_o54620_gradient_1_g,p_o54620_gradient_1_b,p_o54620_gradient_1_a), ((x-p_o54620_gradient_0_pos)/(p_o54620_gradient_1_pos-p_o54620_gradient_0_pos)));
  } else if (x < p_o54620_gradient_2_pos) {
    return mix(vec4(p_o54620_gradient_1_r,p_o54620_gradient_1_g,p_o54620_gradient_1_b,p_o54620_gradient_1_a), vec4(p_o54620_gradient_2_r,p_o54620_gradient_2_g,p_o54620_gradient_2_b,p_o54620_gradient_2_a), ((x-p_o54620_gradient_1_pos)/(p_o54620_gradient_2_pos-p_o54620_gradient_1_pos)));
  }
  return vec4(p_o54620_gradient_2_r,p_o54620_gradient_2_g,p_o54620_gradient_2_b,p_o54620_gradient_2_a);
}
const float p_o9645_gradient_0_pos = 0.000000000;
const float p_o9645_gradient_0_r = 0.000000000;
const float p_o9645_gradient_0_g = 0.000000000;
const float p_o9645_gradient_0_b = 0.000000000;
const float p_o9645_gradient_0_a = 1.000000000;
const float p_o9645_gradient_1_pos = 0.298995694;
const float p_o9645_gradient_1_r = 0.462632060;
const float p_o9645_gradient_1_g = 0.462632060;
const float p_o9645_gradient_1_b = 0.462632060;
const float p_o9645_gradient_1_a = 1.000000000;
const float p_o9645_gradient_2_pos = 0.844450240;
const float p_o9645_gradient_2_r = 1.000000000;
const float p_o9645_gradient_2_g = 1.000000000;
const float p_o9645_gradient_2_b = 1.000000000;
const float p_o9645_gradient_2_a = 1.000000000;
const float p_o9645_gradient_3_pos = 1.000000000;
const float p_o9645_gradient_3_r = 0.000000000;
const float p_o9645_gradient_3_g = 0.000000000;
const float p_o9645_gradient_3_b = 0.000000000;
const float p_o9645_gradient_3_a = 1.000000000;
vec4 o9645_gradient_gradient_fct(float x) {
  if (x < p_o9645_gradient_0_pos) {
    return vec4(p_o9645_gradient_0_r,p_o9645_gradient_0_g,p_o9645_gradient_0_b,p_o9645_gradient_0_a);
  } else if (x < p_o9645_gradient_1_pos) {
    return mix(mix(vec4(p_o9645_gradient_1_r,p_o9645_gradient_1_g,p_o9645_gradient_1_b,p_o9645_gradient_1_a), vec4(p_o9645_gradient_2_r,p_o9645_gradient_2_g,p_o9645_gradient_2_b,p_o9645_gradient_2_a), (x-p_o9645_gradient_1_pos)/(p_o9645_gradient_2_pos-p_o9645_gradient_1_pos)), mix(vec4(p_o9645_gradient_0_r,p_o9645_gradient_0_g,p_o9645_gradient_0_b,p_o9645_gradient_0_a), vec4(p_o9645_gradient_1_r,p_o9645_gradient_1_g,p_o9645_gradient_1_b,p_o9645_gradient_1_a), (x-p_o9645_gradient_0_pos)/(p_o9645_gradient_1_pos-p_o9645_gradient_0_pos)), 1.0-0.5*(x-p_o9645_gradient_0_pos)/(p_o9645_gradient_1_pos-p_o9645_gradient_0_pos));
  } else if (x < p_o9645_gradient_2_pos) {
    return 0.5*(mix(vec4(p_o9645_gradient_1_r,p_o9645_gradient_1_g,p_o9645_gradient_1_b,p_o9645_gradient_1_a), vec4(p_o9645_gradient_2_r,p_o9645_gradient_2_g,p_o9645_gradient_2_b,p_o9645_gradient_2_a), (x-p_o9645_gradient_1_pos)/(p_o9645_gradient_2_pos-p_o9645_gradient_1_pos)) + mix(mix(vec4(p_o9645_gradient_0_r,p_o9645_gradient_0_g,p_o9645_gradient_0_b,p_o9645_gradient_0_a), vec4(p_o9645_gradient_1_r,p_o9645_gradient_1_g,p_o9645_gradient_1_b,p_o9645_gradient_1_a), (x-p_o9645_gradient_0_pos)/(p_o9645_gradient_1_pos-p_o9645_gradient_0_pos)), mix(vec4(p_o9645_gradient_2_r,p_o9645_gradient_2_g,p_o9645_gradient_2_b,p_o9645_gradient_2_a), vec4(p_o9645_gradient_3_r,p_o9645_gradient_3_g,p_o9645_gradient_3_b,p_o9645_gradient_3_a), (x-p_o9645_gradient_2_pos)/(p_o9645_gradient_3_pos-p_o9645_gradient_2_pos)), 0.5-0.5*cos(3.14159265359*(x-p_o9645_gradient_1_pos)/(p_o9645_gradient_2_pos-p_o9645_gradient_1_pos))));
  } else if (x < p_o9645_gradient_3_pos) {
    return mix(mix(vec4(p_o9645_gradient_1_r,p_o9645_gradient_1_g,p_o9645_gradient_1_b,p_o9645_gradient_1_a), vec4(p_o9645_gradient_2_r,p_o9645_gradient_2_g,p_o9645_gradient_2_b,p_o9645_gradient_2_a), (x-p_o9645_gradient_1_pos)/(p_o9645_gradient_2_pos-p_o9645_gradient_1_pos)), mix(vec4(p_o9645_gradient_2_r,p_o9645_gradient_2_g,p_o9645_gradient_2_b,p_o9645_gradient_2_a), vec4(p_o9645_gradient_3_r,p_o9645_gradient_3_g,p_o9645_gradient_3_b,p_o9645_gradient_3_a), (x-p_o9645_gradient_2_pos)/(p_o9645_gradient_3_pos-p_o9645_gradient_2_pos)), 0.5+0.5*(x-p_o9645_gradient_2_pos)/(p_o9645_gradient_3_pos-p_o9645_gradient_2_pos));
  }
  return vec4(p_o9645_gradient_3_r,p_o9645_gradient_3_g,p_o9645_gradient_3_b,p_o9645_gradient_3_a);
}
const float p_o9626_sides = 2.000000000;
const float p_o9626_radius = 0.906667000;
const float p_o9626_edge = 1.000000000;
const float p_o9627_curve_0_x = 0.000000000;
const float p_o9627_curve_0_y = 0.000000000;
const float p_o9627_curve_0_ls = 0.000000000;
const float p_o9627_curve_0_rs = -0.058356000;
const float p_o9627_curve_1_x = 0.101763003;
const float p_o9627_curve_1_y = 0.000000000;
const float p_o9627_curve_1_ls = 0.000000000;
const float p_o9627_curve_1_rs = 0.172531000;
const float p_o9627_curve_2_x = 0.697115004;
const float p_o9627_curve_2_y = 0.763116002;
const float p_o9627_curve_2_ls = -0.114467000;
const float p_o9627_curve_2_rs = -0.042517000;
const float p_o9627_curve_3_x = 1.000000000;
const float p_o9627_curve_3_y = 0.000000000;
const float p_o9627_curve_3_ls = -0.032001000;
const float p_o9627_curve_3_rs = -0.000000000;
float o9627_curve_curve_fct(float x) {
if (x <= p_o9627_curve_1_x) {
float dx = x - p_o9627_curve_0_x;
float d = p_o9627_curve_1_x - p_o9627_curve_0_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o9627_curve_0_y;
float yac = p_o9627_curve_0_y + d*p_o9627_curve_0_rs;
float ybc = p_o9627_curve_1_y - d*p_o9627_curve_1_ls;
float y2 = p_o9627_curve_1_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o9627_curve_2_x) {
float dx = x - p_o9627_curve_1_x;
float d = p_o9627_curve_2_x - p_o9627_curve_1_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o9627_curve_1_y;
float yac = p_o9627_curve_1_y + d*p_o9627_curve_1_rs;
float ybc = p_o9627_curve_2_y - d*p_o9627_curve_2_ls;
float y2 = p_o9627_curve_2_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o9627_curve_3_x) {
float dx = x - p_o9627_curve_2_x;
float d = p_o9627_curve_3_x - p_o9627_curve_2_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o9627_curve_2_y;
float yac = p_o9627_curve_2_y + d*p_o9627_curve_2_rs;
float ybc = p_o9627_curve_3_y - d*p_o9627_curve_3_ls;
float y2 = p_o9627_curve_3_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
}
float o9625_input_d(vec2 uv) {
vec4 o54620_0_1_rgba = o54620_gradient_gradient_fct(fract(p_o54620_repeat*0.15915494309*atan((uv).y-0.5, (uv).x-0.5)));
float o9549_0_1_f = p_o9549_color;
float o9627_0_1_f = o9627_curve_curve_fct(o9549_0_1_f);
float o9626_0_1_f = shape_circle((uv), p_o9626_sides, p_o9626_radius*o9627_0_1_f, p_o9626_edge*1.0);
vec4 o9645_0_1_rgba = o9645_gradient_gradient_fct(o9626_0_1_f);
vec4 o57402_0_s1 = o54620_0_1_rgba;
vec4 o57402_0_s2 = o9645_0_1_rgba;
float o57402_0_a = p_o57402_amount*1.0;
vec4 o57402_0_2_rgba = vec4(blend_multiply((uv), o57402_0_s1.rgb, o57402_0_s2.rgb, o57402_0_a*o57402_0_s1.a), min(1.0, o57402_0_s2.a+o57402_0_a*o57402_0_s1.a));

return (dot((o57402_0_2_rgba).rgb, vec3(1.0))/3.0);
}
vec2 o9625_slope(vec2 uv, float epsilon) {
	return vec2(o9625_input_d(fract(uv+vec2(epsilon, 0.0)))-o9625_input_d(fract(uv-vec2(epsilon, 0.0))), o9625_input_d(fract(uv+vec2(0.0, epsilon)))-o9625_input_d(fract(uv-vec2(0.0, epsilon))));
}const float p_o9606_default_in1 = 0.000000000;
const float p_o9606_default_in2 = 5.350000000;
const float p_o9603_default_in1 = 0.000000000;
const float p_o9603_default_in2 = 0.000000000;
const float p_o9595_default_in1 = 0.000000000;
const float p_o9595_default_in2 = 0.000000000;
const float p_o9576_amount = 0.025000000;
const float p_o9576_eps = 0.040000000;
float o9576_input_d(vec2 uv) {
vec4 o9596_0 = textureLod(texture_1, (uv)-vec2(p_o9577_translate_x, (- elapsed_time / 3.0)), 0.0);
vec4 o9577_0_1_rgba = o9596_0;

return (dot((o9577_0_1_rgba).rgb, vec3(1.0))/3.0);
}
vec2 o9576_slope(vec2 uv, float epsilon) {
	return vec2(o9576_input_d(fract(uv+vec2(epsilon, 0.0)))-o9576_input_d(fract(uv-vec2(epsilon, 0.0))), o9576_input_d(fract(uv+vec2(0.0, epsilon)))-o9576_input_d(fract(uv-vec2(0.0, epsilon))));
}const float p_o9594_in_min_r = 0.000000000;
const float p_o9594_in_min_g = 0.000000000;
const float p_o9594_in_min_b = 0.000000000;
const float p_o9594_in_min_a = 0.000000000;
const float p_o9594_in_mid_r = 0.574999988;
const float p_o9594_in_mid_g = 0.574999988;
const float p_o9594_in_mid_b = 0.574999988;
const float p_o9594_in_mid_a = 0.500000000;
const float p_o9594_in_max_r = 1.000000000;
const float p_o9594_in_max_g = 1.000000000;
const float p_o9594_in_max_b = 1.000000000;
const float p_o9594_in_max_a = 1.000000000;
const float p_o9594_out_min_r = 0.000000000;
const float p_o9594_out_min_g = 0.000000000;
const float p_o9594_out_min_b = 0.000000000;
const float p_o9594_out_min_a = 0.000000000;
const float p_o9594_out_max_r = 1.000000000;
const float p_o9594_out_max_g = 1.000000000;
const float p_o9594_out_max_b = 1.000000000;
const float p_o9594_out_max_a = 1.000000000;
const float p_o9601_default_in1 = 0.000000000;
const float p_o9601_default_in2 = 0.000000000;
const float p_o9554_default_in1 = 0.000000000;
const float p_o9554_default_in2 = 0.000000000;
const float p_o9552_default_in1 = 0.000000000;
const float p_o9552_default_in2 = 0.000000000;
const float p_o9548_sides = 2.000000000;
const float p_o9548_radius = 0.906667000;
const float p_o9548_edge = 0.610000000;
const float p_o9550_sides = 2.000000000;
const float p_o9550_radius = 0.906667000;
const float p_o9550_edge = 0.610000000;
const float p_o9553_default_in1 = 0.000000000;
const float p_o9553_default_in2 = 2.090000000;
const float p_o9551_default_in1 = 0.000000000;
const float p_o9551_default_in2 = 0.000000000;
const float p_o9607_curve_0_x = 0.000000000;
const float p_o9607_curve_0_y = 0.000000000;
const float p_o9607_curve_0_ls = 0.000000000;
const float p_o9607_curve_0_rs = -0.058356000;
const float p_o9607_curve_1_x = 0.101763003;
const float p_o9607_curve_1_y = 0.000000000;
const float p_o9607_curve_1_ls = 0.000000000;
const float p_o9607_curve_1_rs = 0.172531000;
const float p_o9607_curve_2_x = 0.858973980;
const float p_o9607_curve_2_y = 1.000000000;
const float p_o9607_curve_2_ls = 1.299929000;
const float p_o9607_curve_2_rs = -0.000000000;
const float p_o9607_curve_3_x = 1.000000000;
const float p_o9607_curve_3_y = 1.000000000;
const float p_o9607_curve_3_ls = -0.032001000;
const float p_o9607_curve_3_rs = -0.000000000;
float o9607_curve_curve_fct(float x) {
if (x <= p_o9607_curve_1_x) {
float dx = x - p_o9607_curve_0_x;
float d = p_o9607_curve_1_x - p_o9607_curve_0_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o9607_curve_0_y;
float yac = p_o9607_curve_0_y + d*p_o9607_curve_0_rs;
float ybc = p_o9607_curve_1_y - d*p_o9607_curve_1_ls;
float y2 = p_o9607_curve_1_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o9607_curve_2_x) {
float dx = x - p_o9607_curve_1_x;
float d = p_o9607_curve_2_x - p_o9607_curve_1_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o9607_curve_1_y;
float yac = p_o9607_curve_1_y + d*p_o9607_curve_1_rs;
float ybc = p_o9607_curve_2_y - d*p_o9607_curve_2_ls;
float y2 = p_o9607_curve_2_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o9607_curve_3_x) {
float dx = x - p_o9607_curve_2_x;
float d = p_o9607_curve_3_x - p_o9607_curve_2_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o9607_curve_2_y;
float yac = p_o9607_curve_2_y + d*p_o9607_curve_2_rs;
float ybc = p_o9607_curve_3_y - d*p_o9607_curve_3_ls;
float y2 = p_o9607_curve_3_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
}
const float p_o9555_curve_0_x = 0.000000000;
const float p_o9555_curve_0_y = 0.000000000;
const float p_o9555_curve_0_ls = 0.000000000;
const float p_o9555_curve_0_rs = 34.721781000;
const float p_o9555_curve_1_x = 0.141026005;
const float p_o9555_curve_1_y = 0.965023994;
const float p_o9555_curve_1_ls = 0.000000000;
const float p_o9555_curve_1_rs = -0.000000000;
const float p_o9555_curve_2_x = 0.743589997;
const float p_o9555_curve_2_y = 0.774245024;
const float p_o9555_curve_2_ls = -0.939838000;
const float p_o9555_curve_2_rs = -1.217518000;
const float p_o9555_curve_3_x = 1.000000000;
const float p_o9555_curve_3_y = 0.000000000;
const float p_o9555_curve_3_ls = 0.000000000;
const float p_o9555_curve_3_rs = -0.000000000;
float o9555_curve_curve_fct(float x) {
if (x <= p_o9555_curve_1_x) {
float dx = x - p_o9555_curve_0_x;
float d = p_o9555_curve_1_x - p_o9555_curve_0_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o9555_curve_0_y;
float yac = p_o9555_curve_0_y + d*p_o9555_curve_0_rs;
float ybc = p_o9555_curve_1_y - d*p_o9555_curve_1_ls;
float y2 = p_o9555_curve_1_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o9555_curve_2_x) {
float dx = x - p_o9555_curve_1_x;
float d = p_o9555_curve_2_x - p_o9555_curve_1_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o9555_curve_1_y;
float yac = p_o9555_curve_1_y + d*p_o9555_curve_1_rs;
float ybc = p_o9555_curve_2_y - d*p_o9555_curve_2_ls;
float y2 = p_o9555_curve_2_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o9555_curve_3_x) {
float dx = x - p_o9555_curve_2_x;
float d = p_o9555_curve_3_x - p_o9555_curve_2_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o9555_curve_2_y;
float yac = p_o9555_curve_2_y + d*p_o9555_curve_2_rs;
float ybc = p_o9555_curve_3_y - d*p_o9555_curve_3_ls;
float y2 = p_o9555_curve_3_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
}
const float p_o9602_translate_x = 0.000000000;
const float p_o9573_amount = 0.060000000;
const float p_o9573_eps = 0.145000000;
float o9573_input_d(vec2 uv) {
vec4 o9598_0 = textureLod(texture_2, (uv)-vec2(p_o9578_translate_x, (- elapsed_time / 6.0)), 0.0);
vec4 o9578_0_1_rgba = o9598_0;

return (dot((o9578_0_1_rgba).rgb, vec3(1.0))/3.0);
}
vec2 o9573_slope(vec2 uv, float epsilon) {
	return vec2(o9573_input_d(fract(uv+vec2(epsilon, 0.0)))-o9573_input_d(fract(uv-vec2(epsilon, 0.0))), o9573_input_d(fract(uv+vec2(0.0, epsilon)))-o9573_input_d(fract(uv-vec2(0.0, epsilon))));
}const float p_o9604_amount = 0.025000000;
const float p_o9604_eps = 0.145000000;
const float p_o9605_amount = 0.355000000;
const float p_o9605_eps = 0.100000000;
float o9605_input_d(vec2 uv) {
vec4 o9598_0 = textureLod(texture_2, (uv)-vec2(p_o9578_translate_x, (- elapsed_time / 6.0)), 0.0);
vec4 o9578_0_1_rgba = o9598_0;

return (dot((o9578_0_1_rgba).rgb, vec3(1.0))/3.0);
}
vec2 o9605_slope(vec2 uv, float epsilon) {
	return vec2(o9605_input_d(fract(uv+vec2(epsilon, 0.0)))-o9605_input_d(fract(uv-vec2(epsilon, 0.0))), o9605_input_d(fract(uv+vec2(0.0, epsilon)))-o9605_input_d(fract(uv-vec2(0.0, epsilon))));
}float o9604_input_d(vec2 uv) {
vec2 o9605_0_slope = o9605_slope((uv), p_o9605_eps);
vec2 o9605_0_warp = o9605_0_slope;vec4 o9605_0_1_rgba = vec4(sin(((uv)+p_o9605_amount*o9605_0_warp).x*20.0)*0.5+0.5, sin(((uv)+p_o9605_amount*o9605_0_warp).y*20.0)*0.5+0.5, 0, 1);

return (dot((o9605_0_1_rgba).rgb, vec3(1.0))/3.0);
}
vec2 o9604_slope(vec2 uv, float epsilon) {
	return vec2(o9604_input_d(fract(uv+vec2(epsilon, 0.0)))-o9604_input_d(fract(uv-vec2(epsilon, 0.0))), o9604_input_d(fract(uv+vec2(0.0, epsilon)))-o9604_input_d(fract(uv-vec2(0.0, epsilon))));
}

void fragment() {
	vec2 uv = fract(UV);
vec2 o305707_0_slope = o305707_slope((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y))), p_o305707_eps);
vec2 o305707_0_warp = o305707_0_slope*(1.0-o305707_input_d((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))));vec2 o277932_0_slope = o277932_slope(((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o305707_amount*o305707_0_warp), p_o277932_eps);
vec2 o277932_0_warp = o277932_0_slope*(1.0-o277932_input_d(((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o305707_amount*o305707_0_warp)));float o9549_0_1_f = p_o9549_color;
float o119625_0_1_f = o119625_curve_curve_fct(o9549_0_1_f);
float o262174_0_clamp_false = o119625_0_1_f/p_o262174_default_in2;
float o262174_0_clamp_true = clamp(o262174_0_clamp_false, 0.0, 1.0);
float o262174_0_2_f = o262174_0_clamp_false;
float o154404_0_1_f = shape_circle((((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o305707_amount*o305707_0_warp)+p_o277932_amount*o277932_0_warp), p_o154404_sides, p_o154404_radius*o262174_0_2_f, p_o154404_edge*1.0);
vec4 o277932_0_1_rgba = vec4(vec3(o154404_0_1_f), 1.0);
vec4 o305707_0_1_rgba = o277932_0_1_rgba;
vec2 o9625_0_slope = o9625_slope((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y))), p_o9625_eps);
vec2 o9625_0_warp = o9625_0_slope;vec2 o9576_0_slope = o9576_slope(((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp), p_o9576_eps);
vec2 o9576_0_warp = o9576_0_slope;float o9549_0_3_f = p_o9549_color;
float o9548_0_1_f = shape_circle((((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp)+p_o9576_amount*o9576_0_warp), p_o9548_sides, p_o9548_radius*o9549_0_3_f, p_o9548_edge*1.0);
float o9607_0_1_f = o9607_curve_curve_fct(o9549_0_3_f);
float o9551_0_clamp_false = o9607_0_1_f-p_o9551_default_in2;
float o9551_0_clamp_true = clamp(o9551_0_clamp_false, 0.0, 1.0);
float o9551_0_2_f = o9551_0_clamp_false;
float o9553_0_clamp_false = o9551_0_2_f/p_o9553_default_in2;
float o9553_0_clamp_true = clamp(o9553_0_clamp_false, 0.0, 1.0);
float o9553_0_2_f = o9553_0_clamp_false;
float o9550_0_1_f = shape_circle((((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp)+p_o9576_amount*o9576_0_warp), p_o9550_sides, p_o9550_radius*o9553_0_2_f, p_o9550_edge*1.0);
float o9552_0_clamp_false = o9548_0_1_f-o9550_0_1_f;
float o9552_0_clamp_true = clamp(o9552_0_clamp_false, 0.0, 1.0);
float o9552_0_1_f = o9552_0_clamp_false;
float o9555_0_1_f = o9555_curve_curve_fct(o9549_0_3_f);
float o9554_0_clamp_false = o9552_0_1_f*o9555_0_1_f;
float o9554_0_clamp_true = clamp(o9554_0_clamp_false, 0.0, 1.0);
float o9554_0_1_f = o9554_0_clamp_false;
vec4 o9598_0 = textureLod(texture_2, (((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp)+p_o9576_amount*o9576_0_warp)-vec2(p_o9602_translate_x, (- elapsed_time / 12.0)), 0.0);
vec4 o9602_0_1_rgba = o9598_0;
float o9601_0_clamp_false = o9554_0_1_f*(dot((o9602_0_1_rgba).rgb, vec3(1.0))/3.0);
float o9601_0_clamp_true = clamp(o9601_0_clamp_false, 0.0, 1.0);
float o9601_0_1_f = o9601_0_clamp_false;
vec4 o9594_0_1_rgba = adjust_levels(vec4(vec3(o9601_0_1_f), 1.0), vec4(p_o9594_in_min_r, p_o9594_in_min_g, p_o9594_in_min_b, p_o9594_in_min_a), vec4(p_o9594_in_mid_r, p_o9594_in_mid_g, p_o9594_in_mid_b, p_o9594_in_mid_a), vec4(p_o9594_in_max_r, p_o9594_in_max_g, p_o9594_in_max_b, p_o9594_in_max_a), vec4(p_o9594_out_min_r, p_o9594_out_min_g, p_o9594_out_min_b, p_o9594_out_min_a), vec4(p_o9594_out_max_r, p_o9594_out_max_g, p_o9594_out_max_b, p_o9594_out_max_a));
vec4 o9576_0_1_rgba = o9594_0_1_rgba;
vec2 o9573_0_slope = o9573_slope(((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp), p_o9573_eps);
vec2 o9573_0_warp = o9573_0_slope;float o9549_0_5_f = p_o9549_color;
float o9548_0_4_f = shape_circle((((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp)+p_o9573_amount*o9573_0_warp), p_o9548_sides, p_o9548_radius*o9549_0_5_f, p_o9548_edge*1.0);
float o9607_0_3_f = o9607_curve_curve_fct(o9549_0_5_f);
float o9551_3_clamp_false = o9607_0_3_f-p_o9551_default_in2;
float o9551_3_clamp_true = clamp(o9551_3_clamp_false, 0.0, 1.0);
float o9551_0_5_f = o9551_3_clamp_false;
float o9553_3_clamp_false = o9551_0_5_f/p_o9553_default_in2;
float o9553_3_clamp_true = clamp(o9553_3_clamp_false, 0.0, 1.0);
float o9553_0_5_f = o9553_3_clamp_false;
float o9550_0_4_f = shape_circle((((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp)+p_o9573_amount*o9573_0_warp), p_o9550_sides, p_o9550_radius*o9553_0_5_f, p_o9550_edge*1.0);
float o9552_2_clamp_false = o9548_0_4_f-o9550_0_4_f;
float o9552_2_clamp_true = clamp(o9552_2_clamp_false, 0.0, 1.0);
float o9552_0_3_f = o9552_2_clamp_false;
float o9555_0_3_f = o9555_curve_curve_fct(o9549_0_5_f);
float o9554_2_clamp_false = o9552_0_3_f*o9555_0_3_f;
float o9554_2_clamp_true = clamp(o9554_2_clamp_false, 0.0, 1.0);
float o9554_0_3_f = o9554_2_clamp_false;
vec4 o9598_1 = textureLod(texture_2, (((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp)+p_o9573_amount*o9573_0_warp)-vec2(p_o9602_translate_x, (- elapsed_time / 12.0)), 0.0);
vec4 o9602_0_3_rgba = o9598_1;
float o9601_2_clamp_false = o9554_0_3_f*(dot((o9602_0_3_rgba).rgb, vec3(1.0))/3.0);
float o9601_2_clamp_true = clamp(o9601_2_clamp_false, 0.0, 1.0);
float o9601_0_3_f = o9601_2_clamp_false;
vec4 o9594_0_3_rgba = adjust_levels(vec4(vec3(o9601_0_3_f), 1.0), vec4(p_o9594_in_min_r, p_o9594_in_min_g, p_o9594_in_min_b, p_o9594_in_min_a), vec4(p_o9594_in_mid_r, p_o9594_in_mid_g, p_o9594_in_mid_b, p_o9594_in_mid_a), vec4(p_o9594_in_max_r, p_o9594_in_max_g, p_o9594_in_max_b, p_o9594_in_max_a), vec4(p_o9594_out_min_r, p_o9594_out_min_g, p_o9594_out_min_b, p_o9594_out_min_a), vec4(p_o9594_out_max_r, p_o9594_out_max_g, p_o9594_out_max_b, p_o9594_out_max_a));
vec4 o9573_0_1_rgba = o9594_0_3_rgba;
float o9595_0_clamp_false = (dot((o9576_0_1_rgba).rgb, vec3(1.0))/3.0)*(dot((o9573_0_1_rgba).rgb, vec3(1.0))/3.0);
float o9595_0_clamp_true = clamp(o9595_0_clamp_false, 0.0, 1.0);
float o9595_0_1_f = o9595_0_clamp_false;
vec2 o9604_0_slope = o9604_slope(((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp), p_o9604_eps);
vec2 o9604_0_warp = o9604_0_slope;float o9549_0_7_f = p_o9549_color;
float o9548_0_7_f = shape_circle((((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp)+p_o9604_amount*o9604_0_warp), p_o9548_sides, p_o9548_radius*o9549_0_7_f, p_o9548_edge*1.0);
float o9607_0_5_f = o9607_curve_curve_fct(o9549_0_7_f);
float o9551_6_clamp_false = o9607_0_5_f-p_o9551_default_in2;
float o9551_6_clamp_true = clamp(o9551_6_clamp_false, 0.0, 1.0);
float o9551_0_8_f = o9551_6_clamp_false;
float o9553_6_clamp_false = o9551_0_8_f/p_o9553_default_in2;
float o9553_6_clamp_true = clamp(o9553_6_clamp_false, 0.0, 1.0);
float o9553_0_8_f = o9553_6_clamp_false;
float o9550_0_7_f = shape_circle((((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp)+p_o9604_amount*o9604_0_warp), p_o9550_sides, p_o9550_radius*o9553_0_8_f, p_o9550_edge*1.0);
float o9552_4_clamp_false = o9548_0_7_f-o9550_0_7_f;
float o9552_4_clamp_true = clamp(o9552_4_clamp_false, 0.0, 1.0);
float o9552_0_5_f = o9552_4_clamp_false;
float o9555_0_5_f = o9555_curve_curve_fct(o9549_0_7_f);
float o9554_4_clamp_false = o9552_0_5_f*o9555_0_5_f;
float o9554_4_clamp_true = clamp(o9554_4_clamp_false, 0.0, 1.0);
float o9554_0_5_f = o9554_4_clamp_false;
vec4 o9598_2 = textureLod(texture_2, (((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y)))+p_o9625_amount*o9625_0_warp)+p_o9604_amount*o9604_0_warp)-vec2(p_o9602_translate_x, (- elapsed_time / 12.0)), 0.0);
vec4 o9602_0_5_rgba = o9598_2;
float o9601_4_clamp_false = o9554_0_5_f*(dot((o9602_0_5_rgba).rgb, vec3(1.0))/3.0);
float o9601_4_clamp_true = clamp(o9601_4_clamp_false, 0.0, 1.0);
float o9601_0_5_f = o9601_4_clamp_false;
vec4 o9604_0_1_rgba = vec4(vec3(o9601_0_5_f), 1.0);
float o9603_0_clamp_false = o9595_0_1_f*(dot((o9604_0_1_rgba).rgb, vec3(1.0))/3.0);
float o9603_0_clamp_true = clamp(o9603_0_clamp_false, 0.0, 1.0);
float o9603_0_1_f = o9603_0_clamp_false;
float o9606_0_clamp_false = o9603_0_1_f*p_o9606_default_in2;
float o9606_0_clamp_true = clamp(o9606_0_clamp_false, 0.0, 1.0);
float o9606_0_2_f = o9606_0_clamp_false;
vec4 o9625_0_1_rgba = vec4(vec3(o9606_0_2_f), 1.0);
vec4 o111395_0_s1 = o305707_0_1_rgba;
vec4 o111395_0_s2 = o9625_0_1_rgba;
float o111395_0_a = p_o111395_amount*1.0;
vec4 o111395_0_2_rgba = vec4(blend_screen((scale((uv), vec2(0.5+p_o425155_cx, 0.5+p_o425155_cy), vec2(p_o425155_scale_x, p_o425155_scale_y))), o111395_0_s1.rgb, o111395_0_s2.rgb, o111395_0_a*o111395_0_s1.a), min(1.0, o111395_0_s2.a+o111395_0_a*o111395_0_s1.a));
vec4 o9579_0_1_rgba = o9579_gradient_gradient_fct((dot((o111395_0_2_rgba).rgb, vec3(1.0))/3.0));
vec4 o9600_0_1_rgba = o9600_f(o9579_0_1_rgba);
vec4 o425155_0_1_rgba = o9600_0_1_rgba;

	vec4 color_tex = o425155_0_1_rgba;
	color_tex = mix(pow((color_tex + vec4(0.055)) * (1.0 / (1.0 + 0.055)),vec4(2.4)),color_tex * (1.0 / 12.92),lessThan(color_tex,vec4(0.04045)));
	ALBEDO = color_tex.rgb;
	ALPHA = color_tex.a;

}



