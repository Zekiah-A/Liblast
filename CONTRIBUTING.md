# Contribution Guide
# Introduction

Looks like you want to lend a helping hand? Great, feel welcomed to read the following sections in order to know how to ask questions and see what you can work on!

By following the guidelines detailed below, you are respecting the time of the developers working on this project and in turn the developers will show that respect again in addressing your issue, assessing changes, and helping you finalize your pull requests.


# What we're looking for!

Due to the early state of the game, we're looking for bug reports and any game breaking exploit you might find by tinkering around the game.

Make sure that you're running the latest version of the game and editor before reporting a bug!

Any suggestions to improve documentation are also welcomed.


## Bug Report Template

Short and descriptive example bug report title

A summary of the issue and the OS environment in which it occurs. If suitable,
include the steps required to reproduce the bug.

1.  This is the first step
2.  This is the second step
3.  Further steps, etc.

Any other information you want to share that is relevant to the issue being
reported. This might include the lines of code that you have identified as
causing the bug, and potential solutions (and your opinions on their merits).

# What if I want to add to the game?

Before making an awesome suggestion check the project's roadmap before making the request because it might already be planned!

Keep in mind the vision of the project and its lore when writing the feature
request, other than that, be extensive in detailing the feature and why it fits
the game!


# Not sure on how to contribute?

Check these tutorials [How to Make a Pull Request](https://makeapullrequest.com/) and [First Timers Only](https://www.firsttimersonly.com/).