<p align="center"><img src="https://git.gieszer.link/unfa/liblast/media/branch/main/Design/Logo.png" alt="Liblast - a Libre Multiplayer FPS Game"></p>

# Liblast

A Libre Multiplayer FPS Game built with Godot game engine and a fully FOSS toolchain.

Primary goals:

1. Create a fun open-source game for everyone to enjoy
2. Prove that libre creative tools suffice to make a good game - we use only open-source software in the production
3. Have fun, get to know each other and grow together with the project

Secondary goals:

1. Push the envelope of open-source FPS games in regards to a coherent design, style, technology and overall quality
2. Enable the game to be playable on lower-end computers, but provide higher fidelity for those who have more powerful hardware
3. Allow joining the game from a web browser for quick sessions, as well as providing downloadable package for residential or portable usage
4. Actively help the development of Godot engine and other open-source tools that we depend on
5. Facilitate 3rd party content via mods
6. Provide optional online accounts to securely store user data and protect their in-game identity

---

Subscribe to the [YouTube channel](https://www.youtube.com/channel/UC1Oi1eXwdr8RlqIslyht5AQ) for upcoming video updates!

[**Watch the latest public playtesting session**](https://youtu.be/e5VvaMthQTQ?t=403)

---

## How To Play

### Download the game

Go to the [releases page](https://codeberg.org/unfa/liblast/releases) and download the latest release of the game. You'll find some instructions and notes there as well.

There's one public dedicated server running at `liblast.unfa.xyz` the game will present the server address upon startup.

To start playing Liblast it's recommended to first host a local game and adjust your mouse sensitivity and other preferences, as well as modify your profle (player name and color). Then connect to a server and play!

### Controls

| Key          | Action         |
|--------------|----------------|
| WASD         | Movement       |
| Mouse        | Camera         |
| Left Click   | Shoot          |
| Space        | Jump           |
| Shift (Hold) | Jetpack        |
| 1 / 2        | Select weapon  |
| T            | Chat with Team |
| Y            | Chat with All  |
| Z            | Zoom           |
| M            | Mute audio     |
| ESC          | Main Menu      |


## Contributing and Getting in touch

Check the [Contribution Guide!](./CONTRIBUTING.md)

If you want to talk to the dev team and discuss the game in an instant manner, go here:
https://chat.unfa.xyz/channel/liblast

## How to Edit the Game

### Get the Godot 4 editor

As Godot 4 haven't had a stable release yet, we're using the development builds achived at TuxFamily.
It's important that everyone uses the same version of the engine, as there's still rapid changes going on.

[Download the latest Godot editor executables and export templates as needed.](https://downloads.tuxfamily.org/godotengine/testing/4.0/)

If you're having issues and you suspect an engine bug, [test with the current nightly builds](https://hugo.pro/projects/godot-builds/
) before reporing the problem to here or in [Godot Github](https://github.com/godotengine/godot).


### GNU/Linux

1. Make sure you have `git` and `git-lfs` installed. Additional tools to make using `git` in the commandline easier that we can recommend are [`tig`](https://github.com/jonas/tig) and [`lazygit`](https://github.com/jesseduffield/lazygit).

2. Clone the Git repository:
```
git clone https://codeberg.org/unfa/liblast.git
```

3. Enter the cloned repository:
```
cd liblast
```

4. Initialize Git-LFS:
```
git lfs install
```

5. Pull the Git-LFS stored files:
```
git lfs pull
git fetch
```

6. Run the Godot editor and import the project located in `liblast/Game/project.godot`


### Windows (untested!)

1. Install Git for Windows: https://gitforwindows.org/


2. Clone the Git repository:
```
git clone https://codeberg.org/unfa/liblast.git
```

3. Open GitBash in the cloned repository (ther should be an option in the context menu in Windows Explorer)


4. Initialize Git-LFS:
```
git lfs install
```

5. Pull the Git-LFS stored files (that includes the bundled Godot editor):
```
git lfs pull
git fetch
```

6. Run the Godot editor and import the project located in `../Game/project.godot`


## What does the name of this project mean?

`Libre` + `Blast` = `Liblast` (pronounced _ˈlɪblɑːst_)

No, it's not a library ;)